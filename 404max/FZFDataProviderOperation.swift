//
//  FZFDataProviderOperation.swift
//  404max
//
//  Created by Максим Дорошенко on 31.01.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

import Foundation

// Random picker provider
class FZFDataProviderOperation: Operation {
    
    let timeStep: TimeInterval
    var curPoint: Point
    var startDate: NSDate?
    var delegate: FZFDataProviderDelegate?
    
    init(timeStep: TimeInterval) {
        self.timeStep = timeStep
        curPoint = Point()
        super.init()
    }
    
    override func main() {
        startDate = NSDate()
        generatePoint()
    }
    
    func generatePoint() {
        delegate?.received(point: curPoint)
        DispatchQueue.main.asyncAfter(deadline: .now() + timeStep) {
            if !self.isCancelled {
                self.curPoint = self.curPoint.next(timeStep: self.timeStep)
                self.generatePoint()
            }
        }
    }
}

extension FZFDataProviderOperation: FZFDataProviderProtocol {
    func streamStart() {
        start()
    }
    
    func streamStop() {
        cancel()
    }
}

