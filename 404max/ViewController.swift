//
//  ViewController.swift
//  404max
//
//  Created by Максим Дорошенко on 30.01.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

import UIKit
import SciChart

var dataProvider: FZFDataProvider?

class ViewController: UIViewController  {
    
    var chartSurface: FZFChartSurface!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // init chart
        let chartSurfaceView = FZFChartSurfaceView(frame: view.bounds)
        chartSurface = FZFChartSurface(view: chartSurfaceView)
        view.addSubview(chartSurface.view)
        
        // default data provider
        dataProvider = FZFDataProvider(surface: chartSurface)
        dataProvider?.randomPickerProvider(timeStep: 1) // delay between points, currently is 1 sec
        dataProvider?.start()
    }
    
    deinit {
        dataProvider?.stop()
    }
}
