//
//  Point.swift
//  404max
//
//  Created by Максим Дорошенко on 31.01.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

import Foundation

struct Point {
    let date: NSDate
    let rate: Double
    
    init() {
        date = NSDate()
        rate = 0.0
    }
    
    init(date: NSDate, rate: Double) {
        self.date = date
        self.rate = rate
    }
    
    func next(timeStep: TimeInterval) -> Point {
        let nextDate = date.addingTimeInterval(timeStep)
        let nextRate = rate + (Double(arc4random()) / Double(UINT32_MAX)) * 2 - 1
        return Point(date: nextDate, rate: nextRate)
    }
}

// Additional task. Point from json resourse.
extension Point {
    init(dictionary: [String: Any]) {
        if let date = dictionary["date"] as? NSDate {
            self.date = date
        } else {
            self.date = NSDate()
        }
        if let rate = dictionary["rate"] as? Double {
            self.rate = rate
        } else {
            self.rate = 0.0
        }
    }
}
