//
//  FZFDataProviderProtocol.swift
//  404max
//
//  Created by Максим Дорошенко on 01.02.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

// Protocol for control points provider
protocol FZFDataProviderProtocol {
    func streamStart()
    func streamStop()
}
