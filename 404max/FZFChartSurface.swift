//
//  FZFChartSurface.swift
//  404max
//
//  Created by max on 31/01/17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

import SciChart

class FZFChartSurface: SCIChartSurface {
    
    let xAxisID = "xAxis"
    let yAxisID = "yAxis"
    let xAxisIntID = "yAxisINT"
    
    var chart: SCIFastMountainRenderableSeries? // renderSeries for main chart
    var toolLine: SCIFastLineRenderableSeries? // renderSeries for horizontal line
    
    override init!(view: SCIChartSurfaceViewBase!) {
        super.init(view: view)
        self.view = view;
        makeColors(surface: self)
        makeAxis(surface: self)
        chart = makeDefaultChartRender()
        attach(chart)
        toolLine = makeToolLineRenderer()
        attach(toolLine)
        chartModifier = pinchModifier()
    }
    
    func makeColors(surface: FZFChartSurface) {
        surface.style.backgroundBrush = SCIBrushSolid(color: UIColor.black)
    }
    
    func makeDefaultChartRender() -> SCIFastMountainRenderableSeries! {
        let renderSeries = SCIFastMountainRenderableSeries()
        renderSeries.xAxisId = xAxisID
        renderSeries.yAxisId = yAxisID
        renderSeries.dataSeries = SCIXyDataSeries(xType: .dateTime, yType: .double)
        renderSeries.style.borderPen = SCIPenSolid(color: UIColor.blue, width: 1.0)
        let fillColor = UIColor(red: 0, green: 0, blue: 1, alpha: 0.5)
        renderSeries.style.areaBrush = SCIBrushSolid(color: fillColor)
        return renderSeries
    }
    
    func makeToolLineRenderer() -> SCIFastLineRenderableSeries! {
        let render = SCIFastLineRenderableSeries()
        render.dataSeries = SCIXyDataSeries(xType: .int16, yType: .double)
        render.xAxisId = xAxisIntID
        render.yAxisId = yAxisID
        render.style.linePen = SCIPenSolid(color: UIColor.white, width: 1.0)
        return render
    }
    
    // MARK: Axis
    func makeAxis(surface: FZFChartSurface) {
        surface.attach(chartXAxis(), isXAxis: true)
        surface.attach(chartYAxis(), isXAxis: false)
        
        // numeric axis for line tool
        surface.attach(toolXAxis(), isXAxis: true)
    }
    
    func chartXAxis() -> SCIAxis2D {
        let axis = SCIDateTimeAxis()
        axis.style = SCIAxisStyle()
        //        xAxis.textFormatting = "dd/MM/yyyy"
        axis.axisId = xAxisID
        axis.visibleRangeLimit = SCIDateRange(dateMin: Date(), max: Date().addingTimeInterval(30))
        axis.growBy = SCIDoubleRange(min: SCIGeneric(5), max: SCIGeneric(5))
        axis.autoRange = .always
        axis.style.majorGridLineBrush = gridPen()
        axis.style.minorGridLineBrush = gridPen()
        return axis
    }
    
    func chartYAxis() -> SCIAxis2D {
        let axis = SCINumericAxis()
        axis.style = SCIAxisStyle()
        axis.axisId = yAxisID
        axis.growBy = SCIDoubleRange(min: SCIGeneric(0.1), max: SCIGeneric(0.5))
        axis.autoRange = .always
        axis.style.majorGridLineBrush = gridPen()
        axis.style.minorGridLineBrush = gridPen()
        return axis
    }
    
    func toolXAxis() -> SCIAxis2D {
        let axis = SCINumericAxis()
        axis.autoRange = .never
        axis.visibleRange = SCIIntegerRange(min: SCIGeneric(0), max: SCIGeneric(1))
        axis.axisId = xAxisIntID
        axis.style.drawMajorGridLines = false
        axis.style.drawMinorGridLines = false
        axis.isVisible = false
        return axis
    }
    
    func gridPen() -> SCIPenSolid {
        let gridColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3)
        return SCIPenSolid(color: gridColor, width: 0.4)
    }
    
    // MARK: modifier, currently not working
    func pinchModifier() -> SCIPinchZoomModifier {
        let pinchZoomModifier = SCIPinchZoomModifier()
        pinchZoomModifier.modifierName = "PinchZoomModifier"
        return pinchZoomModifier
    }
}
