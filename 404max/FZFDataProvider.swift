//
//  FZFDataProvider.swift
//  404max
//
//  Created by max on 31/01/17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

import SciChart

class FZFDataProvider: NSObject {
    
    let chart: SCIXyDataSeries
    let toolLine: SCIXyDataSeries
    var surface: FZFChartSurface?
    
    lazy var queue = OperationQueue()
    var provider: FZFDataProviderProtocol?
    
    init(surface: FZFChartSurface) {
        self.surface = surface
        chart = surface.chart!.dataSeries as! SCIXyDataSeries
        toolLine = surface.toolLine!.dataSeries as! SCIXyDataSeries
        toolLine.appendX(SCIGeneric(0), y: SCIGeneric(0))
        toolLine.appendX(SCIGeneric(1), y: SCIGeneric(0))
        super.init()
    }
    
    func randomPickerProvider(timeStep: TimeInterval) {
        let randomPicker = FZFDataProviderOperation(timeStep: timeStep)
        randomPicker.delegate = self
        provider = randomPicker
    }
    
    func start() {
        provider?.streamStart()
    }
    
    func stop() {
        provider?.streamStop()
    }
}

extension FZFDataProvider: FZFDataProviderDelegate {
    func received(point: Point) {
        let y = SCIGeneric(point.rate)
        chart.appendX(SCIGeneric(point.date), y: y)
        toolLine.update(at: 0, xValue: SCIGeneric(0), yValue: y)
        toolLine.update(at: 1, xValue: SCIGeneric(1), yValue: y)
        surface?.invalidateElement()
    }
}

// MARK: Additional task: network data provider sample
extension FZFDataProvider {
    func networkProvider() {
        let provider = FZFDataProviderWebSockets()
        provider.delegate = self
        self.provider = provider
    }
}
