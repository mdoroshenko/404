//
//  FZFChartSurfaceView.swift
//  404max
//
//  Created by max on 31/01/17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

import SciChart

class FZFChartSurfaceView: SCIChartSurfaceView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        translatesAutoresizingMaskIntoConstraints = true
    }
}
