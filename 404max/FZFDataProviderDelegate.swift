//
//  FZFDataProviderDelegate.swift
//  404max
//
//  Created by Максим Дорошенко on 01.02.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

// Protocol for provide points
protocol FZFDataProviderDelegate {
    func received(point: Point)
}
