//
//  FZFDataProviderWebSockets.swift
//  404max
//
//  Created by Максим Дорошенко on 01.02.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

import Foundation

class FZFDataProviderWebSockets: NSObject {
//    let webSocket: 
    var delegate: FZFDataProviderDelegate?
    
    func dataReceived(data: Data) {
        
        // Something like this
        if let dict = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [String: Any] {
            let point = Point(dictionary: dict!)
            delegate?.received(point: point)
        }
    }
}

extension FZFDataProviderWebSockets: FZFDataProviderProtocol {
    func streamStart() {
//        webSocket.open(url:port:)
    }
    
    func streamStop() {
//        webSocket.close()
    }
}
